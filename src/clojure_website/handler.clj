(ns clojure-website.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [net.cgrand.enlive-html :as html]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]))


(def posts ["one" "two" "three" "four"])

(defn extract-body [html]
  (html/at html [#{:html :body}] html/unwrap))

(html/deftemplate layout "clojure_website/views/layout.html" [title content]
  [#{:title :h1}] (html/content title)
  [:div.content] (html/substitute (extract-body content)))

(def index (html/html-resource "clojure_website/views/index.html"))
(def about (html/html-resource "clojure_website/views/about.html"))


(defn blog [posts]
  (html/at (html/html-resource "clojure_website/views/blog.html")
             [:li] (html/clone-for [post posts] (html/content post))))


(defroutes app-routes
  (GET "/" [] (layout "Front page" index))
  (GET "/about" [] (layout "About page" about))
  (GET "/blog" [] (layout "Posts page" (blog posts)))
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes site-defaults))
